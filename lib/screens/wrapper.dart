import 'package:flutter/material.dart';
import 'package:photo_gallery/models/myuser.dart';
import 'package:photo_gallery/screens/authenticate/authenticate.dart';
import 'package:photo_gallery/screens/home/home.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<MyUser?>(context);

    if (user == null) {
      return Authenticate();
    } else {
      return Home();
    }
  }
}
