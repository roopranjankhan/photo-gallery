import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseService {
  final CollectionReference photoCollection = FirebaseFirestore.instance.collection('photos');
}